import {
    filmis
} from '../db/db.js';

const filmsContainer = document.getElementById('films-surprise');

const maybeShowfilms = () => {
    const filmsContainer = document.getElementById('films-surprise');
    if (filmsContainer.children.length > 0) {
        filmsContainer.classList.toggle('hide');
    } else {
        showFilms()
    }
}

const showFilms = () => {
    document.getElementById('films-surprise').classList.toggle('hide');
    filmis.forEach(element => {

        const div1 = document.createElement('div');
        div1.classList.add('films__div');
        filmsContainer.appendChild(div1);

        const p1 = document.createElement('p');
        const p2 = document.createElement('p');
        const p3 = document.createElement('p');
        p3.classList.add('enlace')
        const p4 = document.createElement('p');
        const imigi = document.createElement('img');

        div1.appendChild(p1);
        div1.appendChild(p2);
        div1.appendChild(p4);
        div1.appendChild(imigi);
        div1.appendChild(p3);

        p1.textContent = `Name: ` + element.title;
        p2.textContent = `Episode: ` + element.episode_id;
        p4.textContent = `Director: ` + element.director;
        p3.innerHTML = `<a href=https://www.amazon.es/Star-Wars-New-Hope-DVD/dp/B08D74JYY9/ref=sr_1_2?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=star+wars+a+new+hope&qid=1636792900&qsid=258-9632806-2468050&sr=8-2&sres=0736435387%2CB08D74JYY9%2C0785193499%2C1302902237%2C1785864602%2C1484709128%2C1594746370%2C0345341465%2C1302911287%2C1465420274%2C1780898134%2CB00U8L4T08%2C0794446280%2C1684053803%2C1405278951%2CB003JAB11U%2CB082YSDHRX%2C1594747911%2C0736434704%2C1797205943>Comprar en amazon</a>`
        imigi.src = element.img;
    });
}

export {
    maybeShowfilms
}