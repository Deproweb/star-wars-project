import {
    imgCharacters
} from '../db/db.js'; //Función;

const inputValue = document.getElementById('searcher');
const divList1 = document.getElementById('container-list');

const printOneCharacter = (list) => {
    divList1.innerHTML = "";
    list.forEach(element => {
        if (element.name.includes(inputValue.value)) {
            const div2 = document.createElement('div');
            div2.classList.add('container-list__div-3');
            divList1.appendChild(div2);
            const p1 = document.createElement('p');
            const p2 = document.createElement('p');
            const p3 = document.createElement('p');
            const p4 = document.createElement('p');
            div2.appendChild(p1);
            div2.appendChild(p2);
            div2.appendChild(p3);
            div2.appendChild(p4);
            p1.textContent = `Name: ` + element.name;
            p2.textContent = `Height: ` + element.height;
            p3.textContent = `Mass: ` + element.mass;
            p4.textContent = `Gender: ` + element.gender;

            imgCharacters.forEach(character => {
                console.log(element.name)
                if (element.name === character.name) {
                    const nuevaImg = document.createElement('img');
                    nuevaImg.src = character.url;
                    div2.appendChild(nuevaImg)
                }
            });
        }
    })
}

const getElementApi = () => {
    fetch("https://swapi.dev/api/people/")
        .then((response) => response.json())
        .then((myJson) => {
            const listCharacters = myJson.results;
            printOneCharacter(listCharacters);
        })
};

export {
    getElementApi
}