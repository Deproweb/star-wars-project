//CharacterList
let page = 0;
let flag = true;

const showCharacters = () => {
    document.querySelector('#loading').classList.toggle('hide');
    page += 1;
    getCharacters(`https://swapi.dev/api/people/?page=${page}`);
}

const showCharactersPrev = () => {
    page -= 1;
    getCharacters(`https://swapi.dev/api/people/?page=${page}`);
}

const printCharacters1to1 = (list) => {
    const divList = document.getElementById('container-list');
    divList.innerHTML = "";


    list.forEach(element => {
        const div = document.createElement('div');
        div.classList.add('container-list__div')
        const p = document.createElement('p');
        divList.appendChild(div);
        div.appendChild(p);
        p.innerHTML = `<a href="https://starwars.fandom.com/es/wiki/${element.name}">${element.name}</a>`
    });


    const containerBtnNext = document.getElementById('container-btn-next')
    if (flag === true) {
        flag = false;
        const buttonBtnPrev = document.createElement('button');
        const buttonBtn = document.createElement('button');
        containerBtnNext.appendChild(buttonBtnPrev);
        containerBtnNext.appendChild(buttonBtn);
        buttonBtnPrev.classList.add("container-general__buttons__button");
        buttonBtn.classList.add("container-general__buttons__button");
        buttonBtnPrev.textContent = "Prev 20!";
        buttonBtn.textContent = "Next 20!";
        buttonBtnPrev.addEventListener('click', showCharactersPrev)
        buttonBtn.addEventListener('click', showCharacters);
    }
}

const getCharacters = (url) => {
    fetch(url)
        .then((response) => response.json())
        .then((myJson) => {
            const listCharacters = myJson.results;
            printCharacters1to1(listCharacters);
            document.querySelector('#loading').classList.toggle('hide');
        })
};

export {
    showCharacters,
    showCharactersPrev
}