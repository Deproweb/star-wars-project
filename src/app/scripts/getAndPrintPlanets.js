// import {imgsPlanets} from '../db/db.js';

let page = 0;
let flag = true;

const showPlanets = (action) => {
    page += 1;
    getPlanets(`https://swapi.dev/api/planets/?page=${page}`);
}
const showPlanetsPrev = () => {
    page -= 1;
    getPlanets(`https://swapi.dev/api/planets/?page=${page}`);
}

const printPlanets1to1 = (list) => {
    const divList = document.getElementById('container-list');
    divList.innerHTML="";
    list.forEach(element => {
        const div = document.createElement('div');
        div.classList.add('container-list__div')
        div.classList.add('container-list__div-2')
        const p = document.createElement('p');
        const p2 = document.createElement('p');
        const p3 = document.createElement('p');
        divList.appendChild(div);
        div.appendChild(p);
        div.appendChild(p3);
        div.appendChild(p2);
        p.textContent = element.name;
        p2.textContent = `Climate: ` + element.climate;
        p3.textContent = `Population: ` + element.population;

        const containerBtnNext = document.getElementById('container-btn-next')
        if (flag === true) {
            flag = false;
            const buttonBtnPrev = document.createElement('button');
            const buttonBtn = document.createElement('button');
            containerBtnNext.appendChild(buttonBtnPrev);
            containerBtnNext.appendChild(buttonBtn);
            buttonBtnPrev.classList.add("container-general__buttons__button");
            buttonBtn.classList.add("container-general__buttons__button");
            buttonBtnPrev.textContent = "Prev 20!";
            buttonBtn.textContent = "Next 20!";
            buttonBtnPrev.addEventListener('click', showPlanetsPrev)
            buttonBtn.addEventListener('click', showPlanets);
        }
    });
}

const getPlanets = (url) => {
    fetch(url)
        .then((response) => response.json())
        .then((myJson) => {
            const listPlanets = myJson.results;
            printPlanets1to1(listPlanets);
        })
};

export {
    showPlanets
}