import {
    showCharacters
} from './scripts/getAndPrintCharacters';
import {
    showPlanets
} from './scripts/getAndPrintPlanets';
import {
    getElementApi
} from './scripts/getElementUnique';
import {
    maybeShowfilms
} from './scripts/getFilm';


// // Styles SCSS (don't remove)
import './styles/styles.scss';

//Variables
const buttonValue = document.getElementById('button-value');

//Funcion typewritter
var i = 0;
var txt = 'A long time ago in a galaxy far, far away...' /* The text */
var speed = 100; /* The speed/duration of the effect in milliseconds */

function typeWriter() {
    if (i < txt.length) {
        document.getElementById('header__h3').innerHTML += txt.charAt(i);
        i++;
        setTimeout(typeWriter, speed);
    }
}

//Funcion limpiar
function clearBox() {
    // document.getElementById('container-list').innerHTML = "";
    // showCharacters('RESET');
    // showPlanets('RESET');
    location.reload()
};




//AddEvents
const addEvents = () => {
    document.querySelector('#characters-list').addEventListener('click', showCharacters);
    document.querySelector('#planets-list').addEventListener('click', showPlanets);
    document.getElementById('container-delete__button').addEventListener('click', clearBox)
    buttonValue.addEventListener('click', getElementApi);
    // document.getElementById('buttonBtn').addEventListener('click', showCharacters);
    document.getElementById('films-show').addEventListener('click', maybeShowfilms)
}

//Windowonload
window.onload = function () {
    typeWriter();
    addEvents();
}