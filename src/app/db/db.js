const filmis = [{
        "title": "A New Hope",
        "episode_id": 4,
        "opening_crawl": "It is a period of civil war.\r\nRebel spaceships, striking\r\nfrom a hidden base, have won\r\ntheir first victory against\r\nthe evil Galactic Empire.\r\n\r\nDuring the battle, Rebel\r\nspies managed to steal secret\r\nplans to the Empire's\r\nultimate weapon, the DEATH\r\nSTAR, an armored space\r\nstation with enough power\r\nto destroy an entire planet.\r\n\r\nPursued by the Empire's\r\nsinister agents, Princess\r\nLeia races home aboard her\r\nstarship, custodian of the\r\nstolen plans that can save her\r\npeople and restore\r\nfreedom to the galaxy....",
        "director": "George Lucas",
        "img": "https://images-na.ssl-images-amazon.com/images/I/81aA7hEEykL.jpg",
    },
    {
        "title": "The Empire Strikes Back",
        "episode_id": 5,
        "opening_crawl": "It is a dark time for the\r\nRebellion. Although the Death\r\nStar has been destroyed,\r\nImperial troops have driven the\r\nRebel forces from their hidden\r\nbase and pursued them across\r\nthe galaxy.\r\n\r\nEvading the dreaded Imperial\r\nStarfleet, a group of freedom\r\nfighters led by Luke Skywalker\r\nhas established a new secret\r\nbase on the remote ice world\r\nof Hoth.\r\n\r\nThe evil lord Darth Vader,\r\nobsessed with finding young\r\nSkywalker, has dispatched\r\nthousands of remote probes into\r\nthe far reaches of space....",
        "director": "Irvin Kershner",
        "img": "https://images-na.ssl-images-amazon.com/images/I/91eOgodm4nL.jpg",
    },
    {
        "title": "Return of the Jedi",
        "episode_id": 6,
        "opening_crawl": "Luke Skywalker has returned to\r\nhis home planet of Tatooine in\r\nan attempt to rescue his\r\nfriend Han Solo from the\r\nclutches of the vile gangster\r\nJabba the Hutt.\r\n\r\nLittle does Luke know that the\r\nGALACTIC EMPIRE has secretly\r\nbegun construction on a new\r\narmored space station even\r\nmore powerful than the first\r\ndreaded Death Star.\r\n\r\nWhen completed, this ultimate\r\nweapon will spell certain doom\r\nfor the small band of rebels\r\nstruggling to restore freedom\r\nto the galaxy...",
        "director": "Richard Marquand",
        "img": "https://images-na.ssl-images-amazon.com/images/I/81g8vEs4ixL.jpg",
    },
    {
        "title": "The Phantom Menace",
        "episode_id": 1,
        "opening_crawl": "Turmoil has engulfed the\r\nGalactic Republic. The taxation\r\nof trade routes to outlying star\r\nsystems is in dispute.\r\n\r\nHoping to resolve the matter\r\nwith a blockade of deadly\r\nbattleships, the greedy Trade\r\nFederation has stopped all\r\nshipping to the small planet\r\nof Naboo.\r\n\r\nWhile the Congress of the\r\nRepublic endlessly debates\r\nthis alarming chain of events,\r\nthe Supreme Chancellor has\r\nsecretly dispatched two Jedi\r\nKnights, the guardians of\r\npeace and justice in the\r\ngalaxy, to settle the conflict....",
        "director": "George Lucas",
        "producer": "Rick McCallum",
        "img": "https://ae01.alicdn.com/kf/HTB1h5pCNXXXXXXiaXXXq6xXFXXX9.jpg",
    },
    {
        "title": "Attack of the Clones",
        "episode_id": 2,
        "opening_crawl": "There is unrest in the Galactic\r\nSenate. Several thousand solar\r\nsystems have declared their\r\nintentions to leave the Republic.\r\n\r\nThis separatist movement,\r\nunder the leadership of the\r\nmysterious Count Dooku, has\r\nmade it difficult for the limited\r\nnumber of Jedi Knights to maintain \r\npeace and order in the galaxy.\r\n\r\nSenator Amidala, the former\r\nQueen of Naboo, is returning\r\nto the Galactic Senate to vote\r\non the critical issue of creating\r\nan ARMY OF THE REPUBLIC\r\nto assist the overwhelmed\r\nJedi....",
        "director": "George Lucas",
        "producer": "Rick McCallum",
        "img": "https://assets.catawiki.nl/assets/2015/12/3/0/6/8/06892b4a-99f2-11e5-9f26-66155cf7b6dc.jpg",
    },
    {
        "title": "Revenge of the Sith",
        "episode_id": 3,
        "opening_crawl": "War! The Republic is crumbling\r\nunder attacks by the ruthless\r\nSith Lord, Count Dooku.\r\nThere are heroes on both sides.\r\nEvil is everywhere.\r\n\r\nIn a stunning move, the\r\nfiendish droid leader, General\r\nGrievous, has swept into the\r\nRepublic capital and kidnapped\r\nChancellor Palpatine, leader of\r\nthe Galactic Senate.\r\n\r\nAs the Separatist Droid Army\r\nattempts to flee the besieged\r\ncapital with their valuable\r\nhostage, two Jedi Knights lead a\r\ndesperate mission to rescue the\r\ncaptive Chancellor....",
        "director": "George Lucas",
        "img": "https://assets.catawiki.nl/assets/2019/4/14/7/a/b/7ab48f13-3e0f-489e-90d6-7b93dcf22064.jpg",
    }
]


const imgsPlanets = [{
        name: "Tatooine",
        url: "https://static.wikia.nocookie.net/esstarwars/images/b/b0/Tatooine_TPM.png/revision/latest?cb=20131214162357"
    },
    {
        name: "Alderaan",
        url: "https://static.wikia.nocookie.net/esstarwars/images/4/4a/Alderaan.jpg/revision/latest?cb=20100723184830"
    },
    {
        name: "Yavin IV",
        url: "https://static.wikia.nocookie.net/esstarwars/images/d/d4/Yavin-4-SWCT.png/revision/latest?cb=20170924222729"
    },
    {
        name: "Hoth",
        url: "https://static.wikia.nocookie.net/esstarwars/images/1/1d/Hoth_SWCT.png/revision/latest?cb=20170802030704"
    },
    {
        name: "Dagobah",
        url: "https://static.wikia.nocookie.net/esstarwars/images/1/1c/Dagobah.jpg/revision/latest?cb=20061117132132"
    },
    {
        name: "Bespin",
        url: "https://static.wikia.nocookie.net/esstarwars/images/2/2c/Bespin_EotECR.png/revision/latest?cb=20170527220537",
    },
    {
        name: "Endor",
        url: "https://static.wikia.nocookie.net/esstarwars/images/2/2c/Bespin_EotECR.png/revision/latest?cb=20170527220537"
    },
    {
        name: "Naboo",
        url: "https://static.wikia.nocookie.net/esstarwars/images/f/f0/Naboo_planet.png/revision/latest?cb=20190928214307",
    },
    {
        name: "Coruscant",
        url: "https://static.wikia.nocookie.net/esstarwars/images/9/92/Coruscant_SWCT.png/revision/latest?cb=20200927225715"
    },
    {
        name: "Kamino",
        url: "https://static.wikia.nocookie.net/esstarwars/images/5/52/Kamino-DB.png/revision/latest?cb=20160913200828"
    },
    {
        name: "Geonosis",
        url: "https://static.wikia.nocookie.net/esstarwars/images/6/6d/Geonosis_AotC.png/revision/latest?cb=20161025214407",
    },
    {
        name: "Utapau",
        url: "https://static.wikia.nocookie.net/esstarwars/images/2/2b/Huida_Obi-Wan.jpg/revision/latest/top-crop/width/360/height/450?cb=20070415212034",
    },
    {
        name: "Mustafar",
        url: "https://static.wikia.nocookie.net/esstarwars/images/6/61/Mustafar-TROSGG.png/revision/latest?cb=20201111174744"
    }
]



const imgCharacters = [{
        name: "Luke Skywalker",
        url: "https://media.contentapi.ea.com/content/dam/star-wars-battlefront-2/images/2019/08/swbf2-refresh-hero-large-heroes-page-luke-skywalker-16x9-xl.jpg.adapt.crop1x1.320w.jpg",

    },
    {
        name: "C-3PO",
        url: "https://static.wikia.nocookie.net/esstarwars/images/3/3f/C-3PO_TLJ_Card_Trader_Award_Card.png",

    },
    {
        name: "R2-D2",
        url: "https://static.wikia.nocookie.net/esstarwars/images/e/eb/ArtooTFA2-Fathead.png",

    },
    {
        name: "Darth Vader",
        url: "https://hipertextual.com/wp-content/uploads/2020/01/hipertextual-star-wars-deseo-mas-grande-darth-vader-podria-hacerse-realidad-muy-pronto-2020659163.jpg",

    },
    {
        name: "Leia Organa",
        url: "https://static.wikia.nocookie.net/esstarwars/images/9/9b/Princessleiaheadwithgun.jpg",

    },
    {
        name: "Owen Lars",
        url: "https://static.wikia.nocookie.net/doblaje/images/8/84/Owen_Lars_Young_STAR_WARS.png/revision/latest?cb=20170930191515&path-prefix=es ",

    },
    {
        name: "Beru Whitesun lars",
        url: "https://static.wikia.nocookie.net/starwars/images/8/84/BeruWhitesunLars.jpg/revision/latest?cb=20070625220148",

    },
    {
        name: "R5-D4",
        url: "https://static.wikia.nocookie.net/esstarwars/images/c/cb/R5-D4_Sideshow.png/revision/latest?cb=20171215233114",

    },
    {
        name: "Biggs Darklighter",
        url: "https://static.wikia.nocookie.net/esstarwars/images/4/43/Biggs.png/revision/latest?cb=20180704193529",

    },
    {
        name: "Obi-Wan Kenobi",
        url: "https://www.cinemascomics.com/wp-content/uploads/2020/10/OBI-WAN-KENOBI.jpg",

    },
    {
        name: "Anakin Skywalker",
        url: "https://static.wikia.nocookie.net/esstarwars/images/9/92/AnakinSkywalker-SWTLC.png/revision/latest?cb=20210417200745"
    },
    {
        name: "Wilhuff Tarkin",
        url: "https://static.wikia.nocookie.net/star-wars-peliculas/images/6/6c/Closeuptarkin.jpg/revision/latest?cb=20150127223041&path-prefix=es",

    },
    {
        name: "Chewbacca",
        url: "https://static.wikia.nocookie.net/esstarwars/images/5/51/Chewbacca_TLJ.PNG/revision/latest?cb=20190419143715",

    },
    {
        name: "Han Solo",
        url: "https://static.wikia.nocookie.net/dominios-encantados/images/1/15/WIKI_HAN_SOLO.png/revision/latest?cb=20150601184034&path-prefix=es",

    },
    {
        name: "Greedo",
        url: "https://static.wikia.nocookie.net/esstarwars/images/c/c6/Greedo.jpg/revision/latest?cb=20190807064733",

    },
    {
        name: "Jabba Desilijic Tiure",
        url: "https://static.wikia.nocookie.net/esstarwars/images/4/4e/Jabba_HS.jpg/revision/latest/top-crop/width/360/height/450?cb=20100216135330",

    },
    {
        name: "Wedge Antilles",
        url: "https://static.wikia.nocookie.net/esstarwars/images/5/51/Wedge_Antilles.jpg/revision/latest?cb=20161008211328",

    },
    {
        name: "Jek Tono Porkins",
        url: "https://pbs.twimg.com/profile_images/614431171460624386/Z0pxKl9v.png",

    },
    {
        name: "Yoda",
        url: "https://static.wikia.nocookie.net/disney/images/9/95/Master_Yoda.png/revision/latest?cb=20151211145348&path-prefix=es",

    },
    {
        name: "Palpatine",
        url: "https://wipy.tv/wp-content/uploads/2020/07/edad-de-palpatine-en-star-wars-1200x720.jpg",
    }
];


export {
    filmis,
    imgsPlanets,
    imgCharacters
}